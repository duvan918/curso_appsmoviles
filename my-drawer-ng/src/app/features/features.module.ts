import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'

import { FeaturesRoutingModule } from './features-routing.module'
import { FeaturesComponent } from './features.component'

@NgModule({
    imports: [NativeScriptCommonModule, FeaturesRoutingModule],
    declarations: [FeaturesComponent],
    schemas: [NO_ERRORS_SCHEMA],
  })
  export class FeaturesModule {}