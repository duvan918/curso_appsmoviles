import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'

import { FeaturesComponent } from './features.component'

const routes: Routes = [
  { path: '', component: FeaturesComponent },
  {
    path: "funcionalidad",
    loadChildren: () =>
      import("~/app/features/funcionalidad/funcionalidad.module").then((m) => m.FuncionalidadModule),
  },
]

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule],
})
export class FeaturesRoutingModule {}
